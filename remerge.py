#!/usr/bin/env python3

# Copyright (c) 2020, Dave Odell <dmo2118@gmail.com>
#
# Permission to use, copy, modify, and/or distribute this software for any 
# purpose with or without fee is hereby granted, provided that the above 
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, 
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM 
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR 
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
# PERFORMANCE OF THIS SOFTWARE.

import os.path
import re
import shutil
import subprocess
import sys
import tempfile

r_line = re.compile(b'[^\n]*\n')

def assert_at(filename, lineno, condition):
	if not condition:
		raise Exception('{}:{}'.format(filename, lineno))

def write_lines(path, lines):
	with open(path, 'wb') as f:
		for line in lines:
			f.write(line)

def merge(a_lines, b_lines, c_lines):
	path = tempfile.mkdtemp()
	try:
		a_path = os.path.join(path, 'a')
		b_path = os.path.join(path, 'b')
		c_path = os.path.join(path, 'c')

		write_lines(a_path, a_lines)
		write_lines(b_path, b_lines)
		write_lines(c_path, c_lines)

		result = subprocess.run(['diff3', '-m', a_path, b_path, c_path], stdout = subprocess.PIPE)
		if result.returncode != 1:
			result.check_returncode()

		return r_line.findall(result.stdout)
	finally:
		shutil.rmtree(path)

filenames = sys.argv[1:]

if not filenames:
	print('Usage:', sys.argv[0], '[files...]')

for filename in sys.argv[1:]:
	lines = []
	a_lines = []
	b_lines = []
	c_lines = []

	out = lines

	lineno = 1
	for line in open(filename, 'rb'):
		prefix = line[0:7]
		if prefix == b'<<<<<<<':
			assert_at(filename, lineno, not a_lines)
			assert_at(filename, lineno, out is lines)
			out = a_lines
		elif prefix == b'|||||||':
			assert_at(filename, lineno, out is a_lines)
			assert_at(filename, lineno, not b_lines)
			out = b_lines
		elif prefix == b'=======':
			assert_at(filename, lineno, out is b_lines)
			assert_at(filename, lineno, not c_lines)
			out = c_lines
		elif prefix == b'>>>>>>>':
			assert_at(filename, lineno, out is c_lines)
			out = lines
			out += merge(a_lines, b_lines, c_lines)
			a_lines = []
			b_lines = []
			c_lines = []
		else:
			out.append(line)
		lineno += 1

	write_lines(filename, lines)
